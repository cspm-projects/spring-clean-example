package co.com.christopher;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ms_springApplication {

    public static void main(String[] args) {
        SpringApplication.run(Ms_springApplication.class, args);
    }

}
