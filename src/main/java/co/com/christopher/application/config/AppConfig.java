package co.com.christopher.application.config;

import co.com.christopher.domain.models.example.gateway.ExampleRepository;
import co.com.christopher.domain.useCases.example.ExampleUseCase;
import co.com.christopher.domain.useCases.example.ExampleUseCaseImpl;
import co.com.christopher.infrastructure.drivenAdapters.example.ExampleDataJPA;
import co.com.christopher.infrastructure.drivenAdapters.example.ExampleDataRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    @Bean
    public ExampleRepository exampleRepository(ExampleDataJPA exampleDataJPA){
        return new ExampleDataRepository(exampleDataJPA);
    }

    @Bean
    public ExampleUseCase exampleUseCase(ExampleRepository exampleRepository){
        return new ExampleUseCaseImpl(exampleRepository);
    }

}
