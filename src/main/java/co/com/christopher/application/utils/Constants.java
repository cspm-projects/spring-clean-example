package co.com.christopher.application.utils;

public class Constants {

    public static final String EXAMPLE_ALREADY_EXISTS = "El item %s ya se encuentra registrado";
    public static final String EXAMPLE_NOT_FOUND = "El item con id %d no ha sido encontrado";

}