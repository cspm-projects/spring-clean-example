package co.com.christopher.application.utils.secrets;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("!local")
public class SecretConfig {

    @Bean
    public SecretDBModel getDbModel(){
        return new SecretDBModel();
    }

}
