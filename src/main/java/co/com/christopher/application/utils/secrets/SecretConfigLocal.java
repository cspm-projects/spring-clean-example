package co.com.christopher.application.utils.secrets;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("local")
public class SecretConfigLocal {

    @Value("${dbName}")
    private String dbName;

    @Value("${port}")
    private String port;

    @Value("${host}")
    private String host;

    @Value("${user}")
    private String userName;

    @Value("${password}")
    private String password;

    @Value("${engine}")
    private String engine;

    @Bean
    public SecretDBModel getSecrets() {
        return new SecretDBModel(userName, password, host,
                port, dbName, engine, "");
    }

}
