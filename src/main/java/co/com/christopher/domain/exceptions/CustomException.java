package co.com.christopher.domain.exceptions;

public class CustomException extends RuntimeException {

    public CustomException(String message) { super(message); }

}
