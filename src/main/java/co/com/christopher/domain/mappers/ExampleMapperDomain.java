package co.com.christopher.domain.mappers;

import co.com.christopher.domain.models.example.Example;
import co.com.christopher.domain.useCases.dtos.requests.ExampleSaveRequest;
import co.com.christopher.domain.useCases.dtos.responses.ExampleResponse;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface ExampleMapperDomain {

    Example requestToEntity(ExampleSaveRequest exampleRequest);

    ExampleResponse entityToResponse(Example exampleEntity);

    List<Example> listRequestToEntity(List<ExampleSaveRequest> requestList);

    List<ExampleResponse> listEntityToResponse(List<Example> entityList);
}
