package co.com.christopher.domain.models.example;


import java.util.Date;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Example {

    private Long id;
    private String name;
    private Date createAt;

}