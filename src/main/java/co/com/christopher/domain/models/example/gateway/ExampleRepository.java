package co.com.christopher.domain.models.example.gateway;

import co.com.christopher.domain.models.example.Example;

import java.util.Optional;
import java.util.List;

public interface ExampleRepository {

    Example save(Example example);

    Optional<Example> findById(Long id);

    List<Example> findAll();

    boolean exampleExistsByName(String name);
}
