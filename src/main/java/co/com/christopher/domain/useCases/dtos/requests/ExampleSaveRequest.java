package co.com.christopher.domain.useCases.dtos.requests;

import lombok.Data;

@Data
public class ExampleSaveRequest {

    private String name;

}
