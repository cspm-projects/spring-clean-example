package co.com.christopher.domain.useCases.dtos.responses;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ExampleResponse {

    private Long id;
    private String name;

}
