package co.com.christopher.domain.useCases.example;

import co.com.christopher.domain.useCases.dtos.requests.ExampleSaveRequest;
import co.com.christopher.domain.useCases.dtos.responses.ExampleResponse;

import java.util.List;

public interface ExampleUseCase {

    ExampleResponse saveExample(ExampleSaveRequest requestExample);

    ExampleResponse findExampleById(Long id);

    List<ExampleResponse> findAll();
}
