package co.com.christopher.domain.useCases.example;

import co.com.christopher.application.utils.Constants;
import co.com.christopher.domain.exceptions.ResourceAlreadyExists;
import co.com.christopher.domain.exceptions.ResourceNotFoundException;
import co.com.christopher.domain.mappers.ExampleMapperDomain;
import co.com.christopher.domain.models.example.gateway.ExampleRepository;
import co.com.christopher.domain.useCases.dtos.requests.ExampleSaveRequest;
import co.com.christopher.domain.useCases.dtos.responses.ExampleResponse;
import lombok.RequiredArgsConstructor;
import org.mapstruct.factory.Mappers;

import java.util.List;

@RequiredArgsConstructor
public class ExampleUseCaseImpl implements ExampleUseCase {

    private static final ExampleMapperDomain exampleMapper = Mappers.getMapper(ExampleMapperDomain.class);
    private final ExampleRepository repository;

    @Override
    public List<ExampleResponse> findAll() {
        return exampleMapper.listEntityToResponse(repository.findAll());
    }

    @Override
    public ExampleResponse saveExample(ExampleSaveRequest requestExample) {
        if (repository.exampleExistsByName(requestExample.getName())) {
            throw new ResourceAlreadyExists(String.format(Constants.EXAMPLE_ALREADY_EXISTS, requestExample.getName()));
        }
        return exampleMapper.entityToResponse(repository.save(exampleMapper.requestToEntity(requestExample)));
    }

    @Override
    public ExampleResponse findExampleById(Long id) {
        return exampleMapper.entityToResponse(repository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format(Constants.EXAMPLE_NOT_FOUND, id))));
    }
}
