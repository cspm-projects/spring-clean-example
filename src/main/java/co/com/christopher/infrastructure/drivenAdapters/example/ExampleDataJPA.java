package co.com.christopher.infrastructure.drivenAdapters.example;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ExampleDataJPA extends JpaRepository<ExampleData, Long> {

    boolean existsByName(String name);

    Optional<ExampleData> findByNameLike(String name);

}
