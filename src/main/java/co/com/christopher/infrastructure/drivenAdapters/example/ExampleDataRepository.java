package co.com.christopher.infrastructure.drivenAdapters.example;

import co.com.christopher.domain.models.example.Example;
import co.com.christopher.domain.models.example.gateway.ExampleRepository;
import co.com.christopher.infrastructure.drivenAdapters.mappers.ExampleMapper;
import lombok.RequiredArgsConstructor;
import org.mapstruct.factory.Mappers;

import java.util.Optional;
import java.util.List;

@RequiredArgsConstructor
public class ExampleDataRepository implements ExampleRepository {

    private static final ExampleMapper exampleMapper = Mappers.getMapper(ExampleMapper.class);
    private final ExampleDataJPA repositoryJPA;

    @Override
    public Example save(Example exampleFromRequest) {
        return exampleMapper.dataToEntity(repositoryJPA.save(exampleMapper.entityToData(exampleFromRequest)));
    }

    @Override
    public Optional<Example> findById(Long id) {
        return repositoryJPA.findById(id)
                .map(exampleMapper::dataToEntity);
    }

    @Override
    public boolean exampleExistsByName(String exampleName) {
        return repositoryJPA.existsByName(exampleName);
    }

    @Override
    public List<Example> findAll() {
        return exampleMapper.listDataToEntity(repositoryJPA.findAll());
    }

}
