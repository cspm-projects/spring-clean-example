package co.com.christopher.infrastructure.drivenAdapters.mappers;

import co.com.christopher.domain.models.example.Example;
import co.com.christopher.infrastructure.drivenAdapters.example.ExampleData;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface ExampleMapper {
    
    Example dataToEntity(ExampleData data);

    ExampleData entityToData(Example entity);

    List<Example> listDataToEntity(List<ExampleData> exampleDataList);

    List<ExampleData> listEntityToData(List<Example> exampleEntityList);
}
