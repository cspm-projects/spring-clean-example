package co.com.christopher.infrastructure.entryPoints.example;

import co.com.christopher.domain.useCases.example.ExampleUseCase;
import co.com.christopher.domain.useCases.dtos.requests.ExampleSaveRequest;
import co.com.christopher.domain.useCases.dtos.responses.ExampleResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/examples")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Validated
public class ExampleController {

    private final ExampleUseCase exampleUseCase;

    @PostMapping("")
    public ExampleResponse saveExample(@Valid @RequestBody ExampleSaveRequest exampleRequest) {
        return exampleUseCase.saveExample(exampleRequest);
    }

    @GetMapping("/{id}")
    public ExampleResponse findExampleById(@PathVariable Long id) {
        return exampleUseCase.findExampleById(id);
    }

    @GetMapping("")
    public List<ExampleResponse> findAllExamples() {
        return exampleUseCase.findAll();
    }

}
