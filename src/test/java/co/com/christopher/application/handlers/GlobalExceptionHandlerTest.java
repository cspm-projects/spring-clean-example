package co.com.christopher.application.handlers;

import co.com.christopher.domain.exceptions.CustomException;
import co.com.christopher.domain.exceptions.ResourceAlreadyExists;
import co.com.christopher.domain.exceptions.ResourceNotFoundException;
import co.com.christopher.domain.useCases.dtos.ExceptionResponse;
import co.com.christopher.utils.BeanTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.util.Collections;

@ExtendWith(MockitoExtension.class)
public class GlobalExceptionHandlerTest {

    private String generalError = "Ha ocurrido un error";
    private String fieldErrorFormat = "El campo %s %s";
    private String internalServerError = "INTERNAL_SERVER_ERROR";
    private String badRequest = "BAD_REQUEST";
    private String notFound = "NOT_FOUND";
    private String conflict = "CONFLICT";

    @InjectMocks
    private GlobalExceptionHandler globalExceptionHandler;

    @Test
    public void handleAllExceptionsTest() {
        String messageExpected = "error";

        ResponseEntity<Object> response = globalExceptionHandler.handleAllExceptions(new Exception(messageExpected));
        ExceptionResponse responseBody = (ExceptionResponse) response.getBody();
        assert responseBody != null;
        Assertions.assertEquals(generalError, responseBody.getMessages().get(0));
        Assertions.assertEquals(internalServerError, responseBody.getErrorCode());
        Assertions.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
    }

    @Test
    public void resourceNotFoundTest() {
        String messageExpected = "Not found error";

        ResponseEntity<ExceptionResponse> response = globalExceptionHandler.resourceNotFound(new ResourceNotFoundException(messageExpected));
        ExceptionResponse responseBody = response.getBody();
        assert responseBody != null;

        Assertions.assertEquals(Collections.singletonList(messageExpected), responseBody.getMessages());
        Assertions.assertEquals(notFound, responseBody.getErrorCode());
        Assertions.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());

    }

    @Test
    public void notReadableExceptionTest() {
        String messageExpected = "Request mal formada";

        ResponseEntity<ExceptionResponse> response = globalExceptionHandler.notReadableException(new HttpMessageNotReadableException(messageExpected));
        ExceptionResponse responseBody = response.getBody();
        assert responseBody != null;

        Assertions.assertEquals(Collections.singletonList(messageExpected), responseBody.getMessages());
        Assertions.assertEquals(badRequest, responseBody.getErrorCode());
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());

    }

    @Test
    public void resourceAlreadyExistsTest() {
        String messageExpected = "Conflict";

        ResponseEntity<ExceptionResponse> response = globalExceptionHandler.resourceAlreadyExists(new ResourceAlreadyExists(messageExpected));
        ExceptionResponse responseBody = response.getBody();
        assert responseBody != null;

        Assertions.assertEquals(Collections.singletonList(messageExpected), responseBody.getMessages());
        Assertions.assertEquals(conflict, responseBody.getErrorCode());
        Assertions.assertEquals(HttpStatus.CONFLICT, response.getStatusCode());

    }

    @Test
    public void customExceptionTest() {
        String messageExpected = "Bad request";

        ResponseEntity<ExceptionResponse> response = globalExceptionHandler.customException(new CustomException(messageExpected));
        ExceptionResponse responseBody = response.getBody();
        assert responseBody != null;

        Assertions.assertEquals(Collections.singletonList(messageExpected), responseBody.getMessages());
        Assertions.assertEquals(badRequest, responseBody.getErrorCode());
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());

    }


    @Test
    public void handleMethodArgumentNotValidTest() {
        ResponseEntity<Object> response;
        response = globalExceptionHandler.handleMethodArgumentNotValid(getMethodArgumentNotValidException());
        ExceptionResponse responseBody = (ExceptionResponse) response.getBody();
        assert responseBody != null;

        Assertions.assertEquals(badRequest, responseBody.getErrorCode());
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());

    }

    private MethodArgumentNotValidException getMethodArgumentNotValidException() {
        BeanPropertyBindingResult errors
                = new BeanPropertyBindingResult(new BeanTest(), "testBean");
        errors.rejectValue("name", "invalid");
        return new MethodArgumentNotValidException(null, errors);
    }

}

