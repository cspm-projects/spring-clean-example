package co.com.christopher.utils;

public class BeanTest {

    private String name;

    public BeanTest(String name) {
        this.name = name;
    }

    public BeanTest() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
